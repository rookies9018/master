package com.git.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Users {

	@Id
	@GeneratedValue
	private int userId;
	private String userName;
	private String password;
	private String type;
	private String emailId;
	
	
	
	public Users() {
		super();
	}
	public Users(int userId, String userName, String password, String type, String emailId) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.type = type;
		this.emailId = emailId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
	@Override
	public String toString() {
		return "Users [userId=" + userId + ", userName=" + userName + ", password=" + password + ", type=" + type
				+ ", emailId=" + emailId + "]";
	}
	
	
	
}
