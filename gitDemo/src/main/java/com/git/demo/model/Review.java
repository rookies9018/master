package com.git.demo.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Review {

	private int bookId;
	@Id
	@GeneratedValue
	private int commentId;
	private String comments;
	
	
	
	
	public Review() {
		super();
	}
	public Review(int bookId, int commentId, String comments) {
		super();
		this.bookId = bookId;
		this.commentId = commentId;
		this.comments = comments;
	}
	public int getBookId() {
		return bookId;
	}
	public void setBookId(int bookId) {
		this.bookId = bookId;
	}
	public int getCommentId() {
		return commentId;
	}
	public void setCommentId(int commentId) {
		this.commentId = commentId;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public String toString() {
		return "Review [bookId=" + bookId + ", commentId=" + commentId + ", comments=" + comments + "]";
	}
	
	

}
